import time

def AddToQue(workfile,url):
	print "addtoque",workfile,url
	f=open(workfile,"at")
	f.write("new"+url)
	#f.write("\n")
	f.close()

def ReadFromQue(workfile,indexfile):
	print "ReadFromQue",workfile,indexfile
	
	#get the index of the line we're working on
	findex = open(indexfile,"rt")
	line = findex.readline().strip()
	print "index line is >",line,"<"
	findex.close()
	if len(line)==0:
		index=0
	else:		
		index=int(line)+1
	print "oldindex:",index
	
	#get the line we're on
	fwork = open (workfile,"rt")
	fwork.seek(index)
	print "seeking to",index
	line = fwork.readline()
	print "new line is >",line,"< (",len(line),")"
	fwork.close()
	
	#save the new index
	index += len(line)
	findex = open(indexfile,"wt")
	findex.write(str(index)+"\n")
	findex.close()
	print "newindex:",index

	return line
	

	

INDEXFILE = 'indexfile.txt'
WORKFILE = "workfile.txt"

for i in range(0,10):
	url = ReadFromQue(WORKFILE,INDEXFILE)
	print "URL: ",url
	AddToQue(WORKFILE,url)
	

