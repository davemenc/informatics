import urllib
import re

INDEXFILE = 'indexfile.txt'
WORKFILE = "workfile.txt"
INITIALURL = 'http://www.menconi.com'
SPIDERLIMIT = 20

def AddToQue(workfile,url):
	f=open(workfile,"at")
	f.write(url)
	#f.write("\n")
	f.close()

def ReadFromQue(workfile,indexfile):
	
	#get the index of the line we're working on
	findex = open(indexfile,"rt")
	line = findex.readline().strip()
	findex.close()
	if len(line)==0:
		index=0
	else:		
		index=int(line)+1
	
	#get the line we're on
	fwork = open (workfile,"rt")
	fwork.seek(index)
	line = fwork.readline()
	fwork.close()
	
	#save the new index
	index += len(line)
	findex = open(indexfile,"wt")
	findex.write(str(index)+"\n")
	findex.close()

	return line
	
def InitWebSpider(starterurl,workfile,indexfile):
#initialize system for a fair test
	#overwrite workfile
	fwork = open(workfile,"wt")
	fwork.write(starterurl+"\n")
	fwork.close()
	
	#overwrite index file
	findex = open(indexfile,"wt")
	findex.write("")
	findex.close()
	
def GetUrlsFromPage(url):
	try:
		UrlHandle = urllib.urlopen(url)
	except IOError:
		print "Couldn't Open ",url
		return []
	html = UrlHandle.read()
	UrlHandle.close()
	links = re.findall('href="(http://.*?)"',html)
	return links
	
def SaveUrls(links,workfile):
	for link in links: 
		AddToQue(workfile,link+"\n")
		
#InitWebSpider(INITIALURL,WORKFILE,INDEXFILE)
for i in range(0,SPIDERLIMIT):
	url = ReadFromQue(WORKFILE,INDEXFILE)
	print "new URL:",i,url
	Links = GetUrlsFromPage(url)
	print "LINKS:",Links
	SaveUrls(Links,WORKFILE)
	

